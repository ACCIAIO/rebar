using System;

namespace Rebar.Events
{
    ///<summary cref="PubSubBoard">
    ///The GlobalBoard is a Publisher-Subscriber Hub to exchange global informations.
    ///It is based on the class Rebar.Events.PubSubBoard.
    ///</summary>
    public static class GlobalBoard
    {
        private static readonly PubSubBoard s_hub = new PubSubBoard();

        internal static PubSubBoard Instance => s_hub;

        ///<summary>
        ///Subscribes to the event of name eventName with the given callback.
        ///</summary>
        public static void Subscribe<T>(string eventName, Action<T> subscription) => s_hub.Subscribe(eventName, subscription);

        ///<summary>
        ///Subscribes to the event of name eventName with the given callback with no arguments.
        ///</summary>
        public static void Subscribe(string eventName, Action subscription) => s_hub.Subscribe(eventName, subscription);

        ///<summary>
        ///Removes a subscription to the event of name eventName.
        ///</summary>
        public static bool Unsubscribe<T>(string eventName, Action<T> subscription) => Unsubscribe(eventName, subscription);
        
        ///<summary>
        ///Removes a subscription to the event of name eventName.
        ///</summary>
        public static void Unsubscribe(string eventName, Action subscription) => Unsubscribe(eventName, subscription);

        ///<summary>
        ///Triggers the event of name eventName, thus calling sequentially all subscribed callbacks.
        ///</summary>
        public static void Trigger<T>(string eventName, T args) => s_hub.Trigger(eventName, args);

        ///<summary>
        ///Triggers the event of name eventName, thus calling sequentially all subscribed callbacks with no arguments.
        ///</summary>
        public static void Trigger(string eventName) => s_hub.Trigger(eventName);
    }
}