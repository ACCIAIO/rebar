using System.Linq;
using System.Collections.Generic;
using System;

namespace Rebar
{
    public static class Renum
    {
        /// <summary>
        /// Returns the number of named values for the given enum type.
        /// </summary>
        public static int Count<T>() where T : Enum => Enum.GetValues(typeof(T)).Length;

        /// <summary>
        /// Returns all named values for the given enum type.
        /// </summary>
        public static IList<T> Values<T>() where T : Enum => Enum.GetValues(typeof(T)) as IList<T>;

        /// <summary>
        /// Parses a given name to a given enum type, ignoring the case if the respective flag is set to true.
        /// Throws ArgumentNullException if name is null, ArgumentException if name is not a valid element name
        /// or OverflowException if name is outside the range of the underlying type of the given enum type.
        /// </summary>
        public static T Parse<T>(string name, bool ignoreCase = false) where T : Enum => (T)Enum.Parse(typeof(T), name, ignoreCase);

        /// <summary>
        /// Parses a given name to a given enum type, ignoring the case if the respective flag is set to true.
        /// Returns false if the parsing failed for any reason, true otherwise.
        /// </summary>
        public static bool TryParse<T>(string name, out T value, bool ignoreCase = false) where T : Enum
        {
            try 
            {
                value = Parse<T>(name, ignoreCase);
                return true;
            }
            catch (Exception e) when (e is ArgumentNullException || e is ArgumentException || e is OverflowException)
            {
                value = default(T);
                return false;
            }
        }

        /// <summary>
        /// Parses a given name to a given enum type, ignoring the case if the respective flag is set to true.
        /// Returns the corresponding enum value if the parsing succeeded, def otherwise.
        /// </summary>
        public static T ParseOrDefault<T>(string name, T def, bool ignoreCase = false) where T : Enum => 
            TryParse(name, out T value, ignoreCase) ? value : def;
    }
}