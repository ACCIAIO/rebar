using System;
using System.Linq;
using System.Collections.Generic;

namespace Rebar
{
    public sealed class Option<T>
    {
#region internal
        public class MissingValueException : Exception
        {
            public MissingValueException() : base() { }
            public MissingValueException(string msg) : base(msg) { }
            public MissingValueException(string msg, Exception exception) : base(msg, exception) { } 
        }
#endregion

#region Static
        ///<summary>
        ///Creates an empty Option for the type T.
        ///</summary>
        public static Option<T> Empty { get; } = new Option<T>();

        ///<summary>
        ///Creates an Option containing the given value, which cannot be null. In case of null values, consider using Option.OfNullable.
        ///</summary>
        public static Option<T> Of(T value)
        {
            if (value == null) 
                throw new NullReferenceException("Tried to build an Option with a null value. Consider using Option.OfNullable instead");
            return new Option<T>(value);
        }

        ///<summary>
        ///Creates an Option containing the given value. If the value is null, it creates an empty Option.
        ///</summary>
        public static Option<T> OfNullable(T value)
        {
            if (value == null) return Empty;
            return new Option<T>(value);
        }

        /// <summary>
        /// Returns true if the given option is null or it's empty, false otherwise.
        /// </summary>
        /// <param name="option">The option to check.</param>
        public static bool IsNullOrEmpty(Option<T> option) => (object)option == null || !option.IsPresent;
#endregion

        private readonly T[] m_value;

        private T Value => IsPresent ? 
                m_value[0] : 
                throw new MissingValueException("Trying to access the value of an empty Option of " + typeof(T).Name);

        ///<summary>
        ///Returns true if this Option contains a value, false otherwise.
        ///</summary>
        public bool IsPresent => 0 != m_value.Length;

        private Option() => m_value = new T[0];

        private Option(T value) => m_value = new T[] { value };

        ///<summary>
        ///Returns the Option containing the value itself if the filter function passes as argument returns true, an Empty Option otherwise.
        ///<summary>
        public Option<T> If(Func<T, bool> filter) 
        {
            if (filter == null)
                throw new NullReferenceException("Cannot apply a null filter to an Option value");
            return IsPresent && filter(Value) ? this : Empty;
        }

        ///<summary>
        ///Execute an action on the value of the optional if there is one. Nothing happens if the Option is empty.
        ///<summary>
        public Option<T> Do(Action<T> action)
        {
            if (action == null)
                throw new NullReferenceException("Cannot apply a null action to an Option value");
            if (IsPresent) action(Value);
            return this;
        }

        public Option<T> OrDo(Action action)
        {
            if (action == null)
                throw new NullReferenceException("Cannot execute a null action");
            if (!IsPresent) action();
            return this;
        }

        ///<summary>
        ///Returns an Option containing the result of the application of the given function to the value in this Option. 
        ///Returns an Empty Option if this one doesn't contain a value.
        ///<summary>
        public Option<U> Select<U>(Func<T, U> function)
        {
            if (function == null)
                throw new NullReferenceException("Cannot apply a null function to an Option value");
            return Select(v => Option<U>.OfNullable(function(v)));
        }

        ///<summary>
        ///Returns the result of the application of the given function to the value in this Option. 
        ///Returns an Empty Option if this one doesn't contain a value.
        ///<summary>
        public Option<U> Select<U>(Func<T, Option<U>> function)
        {
            if (function == null)
                throw new NullReferenceException("Cannot apply a null function to an Option value");
            return IsPresent ? function(Value) : Option<U>.Empty; 
        }

        ///<summary>
        ///Returns the value contained in this Option or the given alternative if it is Empty.
        ///<summary>
        public T Or(T alternative) => IsPresent ? Value : alternative;

        ///<summary>
        ///Returns the value contained in this Option or throws the exception of the given type containing a predefined message if it is Empty. 
        ///The Exception type must have a constructor accepting as its only parameter a string.
        ///<summary>
        public T Or(Func<T> supplier)
        {
            if (supplier == null)
                throw new NullReferenceException("Cannot use a null supplier in a Or call");
            return IsPresent ? Value : supplier();
        } 

        public T OrThrow<E>() where E : Exception => OrThrow<E>("Option of " + typeof(T).Name + " is empty");

        ///<summary>
        ///Returns the value contained in this Option or throws the exception of the given type containing the given message if it is Empty. 
        ///The Exception type must have a constructor accepting as its only parameter a string.
        ///<summary>
        public T OrThrow<E>(string message) where E : Exception => 
            IsPresent ? Value : throw (E)Activator.CreateInstance(typeof(E), message);

        ///<summary>
        ///Returns an IEnumerable containing the value of this Option or an empty one. It can be used to execute Linq queries of actions.
        ///<summary>
        public IEnumerable<T> AsEnumerable() => m_value;

        public override string ToString()
        {
            var str = "";
            if (!IsPresent) str += "Empty ";
            return str + typeof(T).Name + " Option";
        }

        public override bool Equals(object obj)
        {
            var that = obj as Option<T>;
            if (that == null) return obj is T tObj && IsPresent && Value.Equals(tObj);
            else return m_value.SequenceEqual(that.m_value);
        }

        public override int GetHashCode() => m_value.GetHashCode() + 31 ^ IsPresent.GetHashCode();

        public static bool operator ==(Option<T> option1, Option<T> option2)
        {
            return (object)option1 == (object)option2 || 
                    (object)option1 != null && option1.Equals(option2);
        }

        public static bool operator !=(Option<T> option1, Option<T> option2) => !(option1 == option2);

        public static bool operator ==(Option<T> option1, T value) => 
            (object)option1 != null && (object)value != null && option1.Equals(value);

        public static bool operator !=(Option<T> option1, T value) => !(option1 == value);
    }
}