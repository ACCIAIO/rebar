using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace Rebar
{
    public sealed class CyclicEnumerable<T> : IEnumerable<T>
    {
        private readonly IEnumerable<T> m_baseEnumerable;

        internal CyclicEnumerable(IEnumerable<T> enumerable)
        {
            if(null == enumerable)
                throw new ArgumentNullException("A Cyclic enumerable cannot have a null base enumerable");
            if(enumerable is CyclicEnumerable<T>)
                enumerable = (enumerable as CyclicEnumerable<T>).m_baseEnumerable;
            m_baseEnumerable = enumerable;
        }

        public int Count() => int.MaxValue;

        public int Count(System.Func<T, bool> predicate) => 
                0 == m_baseEnumerable.Count(predicate) ? 0 : Count();

        public long LongCount() => long.MaxValue;

        public long LongCount(System.Func<T, bool> predicate) => 
                0 == m_baseEnumerable.Count(predicate) ? 0 : LongCount();

        public T Min() => m_baseEnumerable.Min();

        public TResult Min<TResult>(Func<T, TResult> selector) => m_baseEnumerable.Min(selector);

        public T Max() => m_baseEnumerable.Max();

        public TResult Max<TResult>(System.Func<T, TResult> selector) => m_baseEnumerable.Max(selector);

        public decimal Average(System.Func<T, decimal> selector) => m_baseEnumerable.Average(selector);

        public IEnumerator<T> GetEnumerator()
        {
            IEnumerator<T> enumerator = m_baseEnumerable.GetEnumerator();
            bool hasNext = enumerator.MoveNext();
            while(hasNext)
            {
                if (hasNext) yield return enumerator.Current;
                else  enumerator.Reset();
                hasNext = enumerator.MoveNext();
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}