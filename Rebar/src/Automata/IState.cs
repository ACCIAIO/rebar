namespace Rebar.Automata
{  
    /// <summary>
    /// Base interface for an Automaton state. Exposes the base functionalities used by the StateAutomaton
    /// to handle state transitions and life cycle.
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// Represents the activity of a state. In general, a state which is not current state is not active.
        /// The value of this property should be set exclusively by the StateAutomaton, but its internal implementation 
        /// is up to the user.
        /// </summary>
        /// <value> true if active, false otherwise.</value>
        bool IsActive { get; set; }
        /// <summary>
        /// It represents if a state has finished executing and any subsequent call to Tick() would be pass-through.
        /// It's optional to implement logic to drive this property, but states initiating a sequential transtion must
        /// return true at some point of their life cycle.
        /// /// </summary>
        /// <value></value>
        bool HasEnded { get; }
        /// <summary>
        /// Returns the name of the state. By default the name is the name of the state's type
        /// without any instance of the string "State" in it. For instance, the name of a state 
        /// of type SetupState is "Setup".
        /// </summary>
        string Name => GetType().Name.Replace("State", "");

        /// <summary>
        /// Called when the state becomes the current one during a StateAutomaton tick. It's called before OnTick().
        /// </summary>
        void OnEnter();

        /// <summary>
        /// Called once every Tick() call on the StateAutomaton. It's never called in the same tick of OnExit().
        /// </summary>
        void OnTick();

        /// <summary>
        /// Called when the states transition to another one during a StateAutomaton tick. It's never called in the same 
        /// tick as OnTick().
        /// </summary>
        void OnExit();
    }
}