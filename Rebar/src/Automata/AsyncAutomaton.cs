using System;
using System.Threading;

namespace Rebar.Automata
{
    /// <summary>
    /// This class allows to access the ticking functionalities of a state automaton 
    /// as a separate thread.
    /// </summary>
    public class AsyncAutomaton
    {
        private readonly StateAutomaton _automaton;
        private readonly Action<bool> _lockMechanism;

        private Action _callback;
        private bool _queued;

        /// <summary>
        /// The state the Automaton is currently in.
        /// </summary>
        public IState CurrentState => _automaton.CurrentState;

        /// <summary>
        /// True after calling Tick() and until the operation has been concluded.
        /// </summary>
        public bool IsTicking => _queued;

        internal AsyncAutomaton(StateAutomaton automaton, Action<bool> lockMechanism)
        {
            _automaton = automaton;
            _lockMechanism = lockMechanism;
            _callback = null;
            _queued = false;
        }

        private void AsyncTick(Object stateInfo)
        {
            _automaton.Tick();
            _lockMechanism(false);
            _callback?.Invoke();
            _queued = false;
        }

        /// <summary>
        /// Executes a tick on a separate thread if it's the first time it's being called or the last call has finished executing, 
        /// returning `true`. Calls the given callback when finished. Instantly returns `false` if another ticking operation is 
        /// already queued for execution, ignoring the callback parameter.
        /// </summary>
        /// <param name="callback">The callback to call after ticking.</param>
        public bool Tick(Action callback)
        {
            if (_queued) return false;
            _queued = true;
            _callback = callback;
            _lockMechanism(true);
            return ThreadPool.QueueUserWorkItem(AsyncTick);
        }

        /// <summary>
        /// Equivalent to calling Tick() from the StateAutomaton.
        /// </summary>
        public void Tick() => Tick(null);
    }
}