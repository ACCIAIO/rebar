using System;
using System.Linq;
using System.Collections.Generic;

using Rebar.Events;

namespace Rebar.Automata
{
    /// <summary>
    /// Class representing a state machine: exposes methods to define state transitions where each state 
    /// is a custom class implementing the IState interface. The State Automaton only processes states 
    /// and state transitions on Tick() calls. How often to call the Tick() method is up to the user. 
    /// There are 3 types of transitions: Conditional, General and Sequential. 
    /// </summary>
    public class StateAutomaton 
    {
#region Internal Classes
        private class Transition : IComparable<Transition>
        {
            private readonly Func<bool> _predicate;
            private readonly Func<IState> _toSupplier;
            private readonly IState _to;
            private readonly int _priority;

            public IState TargetState => _to ?? _toSupplier();
            public bool IsDynamic => _toSupplier != null;

            private Transition(Func<IState> toSupplier, IState to, Func<bool> predicate, int priority)
            {
                _toSupplier = toSupplier;
                _to = to;
                _predicate = predicate;
                _priority = priority;
            }

            public Transition(Func<IState> toSupplier, Func<bool> predicate, int priority) : 
                    this(toSupplier, null, predicate, priority) { }

            public Transition(IState to, Func<bool> predicate, int priority) : 
                    this(null, to, predicate, priority) { }

            public bool CheckCondition() => _predicate();

            public int CompareTo(Transition other) => _priority - other._priority;

            public override bool Equals(object obj)
            {
                Transition that = obj as Transition;
                var va = TargetState.Equals(that.TargetState);
                return that != null && (!IsDynamic && !that.IsDynamic && TargetState.Equals(that.TargetState));
            }

            public override int GetHashCode() => base.GetHashCode();
        }
#endregion

#region Static
        /// <summary>
        /// The PubSubHub event name to use to register for state change events. Callbacks' argument is a string
        /// identifying the name of the state the Automaton has transitioned into. This event is raised after the 
        /// call to the specific state's OnEnter().
        /// </summary>
        public const string CHANGE_STATE_EVENT_NAME = "StateAutomaton_event_change";
#endregion

#region Events     
        /// <summary>
        /// Raised whenever a state transition happens and its argument is the name of the state the automaton has transitioned into.
        /// This event is raised after the call to the specific state's OnEnter().
        /// </summary>
        public event Action<string> OnStateChange;
#endregion

        private readonly Dictionary<IState, IState> _sequentialTransitions = new Dictionary<IState, IState>();
        private readonly LinkedList<Transition> _generalTransitions = new LinkedList<Transition>(); 
        private readonly Dictionary<IState, LinkedList<Transition>> _conditionalTransitions = new Dictionary<IState, LinkedList<Transition>>();
        private readonly HashSet<IState> _statesWithBackwardTransitions = new HashSet<IState>();

        private PubSubBoard _pubSubBoard;
        private IState _previous;
        private IState _current;
        private IState _entry;
        private bool _locked;
        private AsyncAutomaton _asyncVersion;

#region Properties
        /// <summary>
        /// The state the Automaton is currently in.
        /// </summary>
        public IState CurrentState => _current;
        /// <summary>
        /// The state the Automaton defaults to the first ever Tick() execution.
        /// If null, Tick() will raise an InvalidOperationException.
        /// </summary>
        /// <value>The state to default to the first execution.</value>
        public IState EntryState 
        {
            get => _entry;
            set 
            {
                if(_statesWithBackwardTransitions.Contains(value))
                    throw new ArgumentException("cannot set as entry state one already used in backward transitions");
                _entry = value;
            }
        }
        /// <summary>
        /// An interface that allows to execute Tick calls on a separate thread
        /// </summary>
        public AsyncAutomaton Async => _asyncVersion ?? (_asyncVersion = new AsyncAutomaton(this, l => _locked = l));
        /// <summary>
        /// true if the Tick() method can be called with no errors, false otherwise.
        /// </summary>
        public bool IsReady => EntryState != null;
#endregion

#region Constructors
        /// <summary>
        /// Creates a new StateAutomaton with EntryState set to the given value and registers
        /// it to the given PubSubBoard.
        /// </summary>
        /// <param name="entry">The state the Automaton will defaults to the first time Tick() is called.</param>
        /// <param name="pubSubBoard">The PubSubBoard to publish state change events onto.</param>
        public StateAutomaton(IState entry, PubSubBoard pubSubBoard)
        {
            EntryState = entry;
            _pubSubBoard = pubSubBoard;
            _previous = null;
            _asyncVersion = null;
            _locked = false;

            OnStateChange += s => _pubSubBoard?.Trigger<string>(CHANGE_STATE_EVENT_NAME, s);
        }

        /// <summary>
        /// Creates a new StateAutomaton with EntryState set to the given value and registers
        /// it to the global board if the given parameter is set to true.
        /// </summary>
        /// <param name="entry">The state the Automaton will defaults to the first time Tick() is called.</param>
        /// <param name="registerToGlobalBoard">true to register the StateAutomaton to the global PubSubBoard, false otherwise.</param>
        public StateAutomaton(IState entry, bool registerToGlobalBoard) : 
                this(entry, registerToGlobalBoard ? GlobalBoard.Instance : null) { }
        
        /// <summary>
        /// Creates a new StateAutomaton with EntryState set to the given value.
        /// </summary>
        /// <param name="entry">The state the Automaton will defaults to the first time Tick() is called.</param>
        public StateAutomaton(IState entry) : this(entry, null) { }

        /// <summary>
        /// Creates a new StateAutomaton. EntryState must be specified explicitly for it to work.
        /// </summary>
        public StateAutomaton() : this(null, null) { }
#endregion

#region Private Methods     
        private void InitializeState(IState state) => state.IsActive = _current == state;

        private void ChangeState(IState state)
        {
            _previous = _current;
            if (_previous != null)
            {
                _previous.OnExit();
                _previous.IsActive = false;
            }

            _current = state;
            _current.IsActive = true;
            _current.OnEnter();

            OnStateChange(_current.Name);
        }

        private bool ExecuteFirstValidTransition(IEnumerable<Transition> transitions, bool excludeSelf)
        {
            foreach (var transition in transitions)
            {
                bool canCheck = !excludeSelf || !transition.TargetState.Equals(CurrentState);
                if (canCheck && transition.CheckCondition())
                {
                    ChangeState(transition.TargetState);
                    return true;
                }
            }
            return false;
        }

        private bool TryStartAutomaton()
        {
            if (CurrentState == null)
            {
                if (!IsReady)
                    throw new InvalidOperationException("can't start the automaton from a null entry state");
                ChangeState(EntryState);
                return true;
            }
            return false;
        }

        private bool TrySequentialTransition()
        {
            if (_sequentialTransitions.ContainsKey(CurrentState) && CurrentState.HasEnded)
            {
                ChangeState(_sequentialTransitions[CurrentState]);
                return true;
            }
            return false;
        }

        private bool TryGeneralTransition() => ExecuteFirstValidTransition(_generalTransitions, true);

        private bool TryConditionalTransition() => _conditionalTransitions.ContainsKey(CurrentState) && 
                ExecuteFirstValidTransition(_conditionalTransitions[CurrentState], false);

        private void AddConditionalTransition(IState from, Transition transition)
        {
            if (!_conditionalTransitions.ContainsKey(from))
                _conditionalTransitions.Add(from, new LinkedList<Transition>());
            if (_conditionalTransitions[from].Contains(transition))
                throw new ArgumentException("Transition already added");
            else _conditionalTransitions[from].AddInOrder(transition);
            
            InitializeState(from);
            if (!transition.IsDynamic) 
                InitializeState(transition.TargetState);
        }
#endregion

        /// <summary>
        /// Makes the automaton publish state change events onto the given PubSubBoard.
        /// </summary>
        /// <param name="psb">The PubSubBoard to publish state change events onto.</param>
        public void PublishStatesEventsIn(PubSubBoard psb) => _pubSubBoard = psb;

        /// <summary>
        /// Makes the automaton publish state change events onto the Global PubSubBoard.
        /// </summary>
        public void PublishStatesEventsInGlobal() => PublishStatesEventsIn(GlobalBoard.Instance);

        /// <summary>
        /// Stops the automaton publishing any more state change events on a PubSubBoard.
        /// </summary>
        public void StopPublishingStatesEvents() => _pubSubBoard = null;

        /// <summary>
        /// Creates a sequential transition from state 'from' to state 'to'.
        /// A sequential transition happens as soon as from.HasEnded is set to true.
        /// </summary>
        /// <param name="from">
        /// The state the transition starts from. If null an ArgumentNullException is thrown.
        /// </param>
        /// <param name="to"> The state the automaton will transition to. If null an ArgumentNullException is thrown.</param>
        public void SetSequentialTransition(IState from, IState to)
        {
            if (from == null)
                throw new ArgumentNullException("from", "Cannot register a transition from a null state");
            if (to == null)
                throw new ArgumentNullException("to", "Cannot register a transition to a null state");
            _sequentialTransitions[from] = to;

            InitializeState(from);
            InitializeState(to);
        }

        /// <summary>
        /// Creates a general transition to state 'to' when the given predicate evaluates to true.
        /// A general transition happens starting from any state but 'to' as soon as the 
        /// given predicate evaluates to true. It has priority over Sequential transitions.
        /// </summary>
        /// <param name="to">The state to transition to. If null an ArgumentNullException is thrown.</param>
        /// <param name="predicate">
        /// The predicate that triggers the transition when evaluated to true. 
        /// If null an ArgumentNullException is thrown.
        /// </param>
        /// <param name="priority">
        /// Although a situation where multiple transitions evaluate to true at the same time
        /// is discouraged, which one to call in case it happens can be decided setting a priority value.
        /// </param>
        public void AddGeneralTransition(IState to, Func<bool> predicate, int priority = 0)
        {
            if (to == null) 
                throw new ArgumentNullException("to", "Cannot register a transition to a null state");
            if (predicate == null) 
                throw new ArgumentNullException("predicate", "Cannot register a transition with a null predicate");
            if (_generalTransitions.Any(t => !t.IsDynamic && t.TargetState == to))
                return;
            _generalTransitions.AddInOrder(new Transition(to, predicate, priority));

            InitializeState(to);
        }

        /// <summary>
        /// Creates a backward transition from state 'from'..
        /// Two states causing backward transitions consecutively will loop the automaton between them.
        /// It has the same priority of Conditional transitions.
        /// </summary>
        /// <param name="from">
        /// The state the transition starts from. If null an ArgumentNullException is thrown. 
        /// If it's the entry state an ArgumentException is thown.
        /// </param>
        /// <param name="predicate">
        /// The predicate that triggers the transition when evaluated to true. 
        /// If null an ArgumentNullException is thrown.
        /// </param>
        /// <param name="priority">
        /// Although a situation where multiple transitions evaluate to true at the same time
        /// is discouraged, which one to call in case it happens can be decided setting a priority value.
        /// </param>
        public void AddBackwardTransition(IState from, Func<bool> predicate, int priority = 0)
        {
            if (from == EntryState)
                throw new ArgumentException($"{from.Name} cannot be used in a backward transition and as entry state");
            if (predicate == null)
                throw new ArgumentNullException("predicate", "Cannot register a transition with a null predicate");
            AddConditionalTransition(from, new Transition(GetEntry, predicate, priority));
            _statesWithBackwardTransitions.Add(from);

            IState GetEntry() => EntryState;
        }

        /// <summary>
        /// Creates a conditional transition from 'from' to 'to' when the given predicate evaluates to true.
        /// It has priority over both Sequential and General transitions.
        /// </summary>
        /// <param name="from">
        /// The state the transition starts from. If null an ArgumentNullException is thrown.
        /// </param>
        /// <param name="to">
        /// The state to transition to. If null an ArgumentNullException is thrown.
        /// </param>
        /// <param name="predicate">
        /// The predicate that triggers the transition when evaluated to true. 
        /// If null an ArgumentNullException is thrown.
        /// </param>
        /// <param name="priority">
        /// Although a situation where multiple transitions evaluate to true at the same time
        /// is discouraged, which one to call in case it happens can be decided setting a priority value.
        /// </param>
        public void AddConditionalTransition(IState from, IState to, Func<bool> predicate, int priority = 0)
        {
            if (to == null)
                throw new ArgumentNullException("to", "Cannot register a transition to a null state");
            AddConditionalTransition(from, new Transition(to, predicate, priority));
        }

        /// <summary>
        /// Ticks the automaton once, transitioning to a new state and calling its Tick() method.
        /// </summary>
        public void Tick()
        {
            if(_locked)
                throw new InvalidOperationException("This state automaton is already being ticked on a separate thread");

            bool stateChanged;
            stateChanged = TryStartAutomaton();
            if (!stateChanged)
                stateChanged = TryConditionalTransition();
            if (!stateChanged)
                stateChanged = TryGeneralTransition();
            if (!stateChanged) 
                stateChanged = TrySequentialTransition();
            CurrentState.OnTick();
        }

        /// <summary>
        /// Clears all transitions and sets the CurrentState and the EntryState to null.
        /// </summary>
        public void Reset() {
            _current = null;
            EntryState = null;

            _conditionalTransitions.Clear();
            _generalTransitions.Clear();
            _sequentialTransitions.Clear();
            _statesWithBackwardTransitions.Clear();
        }
    }
}