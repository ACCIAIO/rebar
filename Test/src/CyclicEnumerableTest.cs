using Xunit;

using Rebar;
using static Rebar.Renumerable;

namespace Test
{
    public class CyclicEnumerableTest
    {
        private readonly CyclicEnumerable<int> m_testValues = new int[] { 0, 1, 2, 3, 4 }.ToCycle();

        [Fact(DisplayName = "Count returns max value or 0 if predicate filters everything out and overrides Linq's call")]
        public void Count_returns_max_value_or_0_if_predicate_filters_everything_out_and_overrides_Linqs_call()
        {
            Assert.Equal(int.MaxValue, m_testValues.Count());
            Assert.Equal(int.MaxValue, m_testValues.Count(v => 0 <= v));
            Assert.Equal(0, m_testValues.Count(v => 0 > v));
        }

        [Fact(DisplayName = "LongCount returns max value or 0 if predicate filters everything out and overrides Linq's call")]
        public void LongCount_returns_max_value_or_0_if_predicate_filters_everything_out_and_overrides_Linqs_call()
        {
            Assert.Equal(long.MaxValue, m_testValues.LongCount());
            Assert.Equal(long.MaxValue, m_testValues.LongCount(v => 0 <= v));
            Assert.Equal(0, m_testValues.LongCount(v => 0 > v));
        }

        [Fact(DisplayName = "Min returns the minimum value and overrides Linq's call")]
        public void Min_returns_the_min_value_and_overrides_Linqs_call()
        {
            Assert.Equal(0, m_testValues.Min());
            Assert.Equal(-2, m_testValues.Min(v => v - 2));
        }

        [Fact(DisplayName = "Max returns the maximum values and overrides Linq's call")]
        public void Max_returns_the_max_value_and_overrides_Linqs_call()
        {
            Assert.Equal(4, m_testValues.Max());
            Assert.Equal(2, m_testValues.Max(v => v - 2));
        }

        [Fact(DisplayName = "Average returns the average value and overrides Linq's call")]
        public void Average_returns_average_value_and_overrides_Linqs_call()
        {
            Assert.Equal(2, m_testValues.Average(v => v));
        }

        [Fact(DisplayName = "A cyclic enumerable repeats the same values of the initial enumerable indefinitely")]
        public void Cyclic_enumerable_repeats_the_same_values_of_the_initial_enumerable_indefinitely()
        {
            var cycle = m_testValues.ToCycle();
            Assert.Equal(int.MaxValue, (int)cycle.Count());
            int i = 0;
            foreach(var v in cycle)
            {
                Assert.Equal(i % 5, v);
                i++;
                if (i >= 15) break;
            }
        }
    }
}