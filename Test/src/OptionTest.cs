using System;
using System.Linq;
using Xunit;

using Rebar;

namespace Test
{
    public class OptionTest
    {
        [Fact(DisplayName = "An Empty Option does not contain a value")]
        public void An_empty_option_does_not_contain_a_value()
        {
            var empty = Option<string>.Empty;

            Assert.False(empty.IsPresent);
            Assert.Equal("", empty.Or(""));

            string shouldBeEmpty = null;
            empty.Do(s => shouldBeEmpty = s).OrDo(() => shouldBeEmpty = "");
            Assert.Equal("", shouldBeEmpty);

            var emptyS = Option<string>.OfNullable(null);

            Assert.False(emptyS.IsPresent);
            Assert.Equal("Nothing in here", emptyS.Or("Nothing in here"));

            shouldBeEmpty = null;
            emptyS.Do(str => shouldBeEmpty = str).OrDo(() => shouldBeEmpty = "");
            Assert.Equal("", shouldBeEmpty);
        }

        [Fact(DisplayName = "A Non-Empty Option allows to retrieve the value inside")]
        public void A_non_empty_option_allows_to_retrieve_the_value_inside()
        {
            var opt = Option<string>.Of("VALUE");

            Assert.True(opt.IsPresent);
            Assert.Equal("VALUE", opt.Or(""));

            string shouldBeVALUE = null;
            opt.Do(s => shouldBeVALUE = s).OrDo(() => shouldBeVALUE = "");
            Assert.Equal("VALUE", shouldBeVALUE);
        }

        [Fact(DisplayName = "An option transformed to enumerable can be iterated on")]
        public void An_option_transformed_to_enumerable_can_be_iterated_on()
        {
            var opt = Option<int>.Of(14);

            foreach(var v in opt.AsEnumerable())
            {
                Assert.Equal(14, v);
            }
        }

        [Fact(DisplayName = "An option equals another if it's the same option")]
        public void An_option_equals_another_if_its_the_same_option()
        {
            var opt1 = Option<int>.Of(14);
            var opt2 = opt1;

            Assert.True(opt1 == opt2);
            Assert.False(opt1 != opt2);
        }

        [Fact(DisplayName = "An option equals another if their value are equal")]
        public void An_option_equals_another_if_their_value_are_equal()
        {
            var opt = Option<int>.Of(14);

            Assert.True(opt == Option<int>.Of(14));
            Assert.False(opt != Option<int>.Of(14));
            Assert.False(opt == Option<int>.Empty);
            Assert.True(opt != Option<int>.Empty);
            Assert.False(opt == Option<int>.Of(9));
            Assert.True(opt != Option<int>.Of(9));
        }

        [Fact(DisplayName = "An option equals another if they are both empty")]
        public void An_option_equals_another_if_they_are_both_empty()
        {
            var optEmpty = Option<int>.Empty;

            Assert.True(optEmpty == Option<int>.Empty);
            Assert.False(optEmpty != Option<int>.Empty);
        }

        [Fact(DisplayName = "An option equals a value if the internal value equals the value used for comparison")]
        public void An_option_equals_a_value_if_the_internal_value_equals_the_value_used_for_comparison()
        {
            Option<int> nullOpt = null;

            Assert.True(Option<int>.Of(14) == 14);
            Assert.False(Option<int>.Of(14) != 14);

            Assert.True(Option<int>.Empty != 14);
            Assert.False(Option<int>.Empty == 14);

            Assert.False(Option<int>.Empty == null);
            Assert.True(Option<int>.Empty != null);

            Assert.True(nullOpt == null);
            Assert.False(nullOpt != null);
        }
    }
}