using System.Linq;
using System.Collections.Generic;
using Xunit;

using Rebar;
using static Rebar.Renumerable;

namespace Test
{
    public class EnumerableTest
    {
        private IEnumerable<int> m_testValues = new int[] { 0, 1, 2, 3, 4 };

        [Fact(DisplayName = "Random sampling on an empty returns type default")]
        public void Random_sampling_on_an_empty_returns_type_default()
        {
            int shouldBeDefault = new int[] {}.RandomSample();
            Assert.True(shouldBeDefault == default(int));
        }

        [Fact(DisplayName = "Random sampling returns an element in the enumerable")]
        public void Random_sampling_returns_an_element_in_the_enumerable()
        {
            int v = m_testValues.RandomSample();    
            Assert.Contains(v, m_testValues);
        }

        [Fact(DisplayName = "Multiple distinct sampling returns distinct elements in the enumerable")]
        public void Multiple_distinct_sampling_returns_distinct_elements_in_the_enumerable()
        {
            IEnumerable<int> samples = m_testValues.RandomSample(2);
            Assert.Collection(samples, v => m_testValues.Contains(v), v => m_testValues.Contains(v));
            Assert.Equal(2, (int)samples.Distinct().Count());
        }

        [Fact(DisplayName = "Multiple sampling with maximum occurrences returns elements in the enumerable")]
        public void Multiple_sampling_with_maximum_occurrences_returns_elements_in_the_enumerable()
        {
            IEnumerable<int> samples = m_testValues.RandomSample(6, 2);
            Assert.Collection(samples, 
                    v => m_testValues.Contains(v), 
                    v => m_testValues.Contains(v),
                    v => m_testValues.Contains(v),
                    v => m_testValues.Contains(v),
                    v => m_testValues.Contains(v),
                    v => m_testValues.Contains(v));
            foreach(var v in samples)
            {
                var count = samples.Count(s => s == v);
                Assert.True(count <= 2);
            }
        }

        [Fact(DisplayName = "Count does not fail counting CyclicEnumerables")]
        public void Count_does_not_fail_counting_CyclicEnumerables()
        {
            var cycle = m_testValues.ToCycle();
            Assert.Equal(int.MaxValue, cycle.Count());
        }

        [Fact(DisplayName = "Shuffling an enumerable returns all elements in it in a random order")]
        public void Shuffling_an_enumerable_returns_all_elements_in_it_in_a_random_order()
        {
            var original = m_testValues.ToList();
            var shuffle = m_testValues.Shuffled();

            foreach(var v in shuffle)
            {
                Assert.Contains(v, original);
                original.Remove(v);
            }

            original = m_testValues.ToList();
            shuffle = m_testValues.FineShuffled();

            foreach(var v in shuffle)
            {
                Assert.Contains(v, original);
                original.Remove(v);
            }
        }

#region Range
        [Fact(DisplayName = "Range(n) should contain all elements from 0 to n - 1")]
        public void Range_n_should_contain_all_elements_from_0_to_n_minus_1()
        {
            var range = Range(5);
            Assert.True(5 == range.Count());
            Assert.Collection(range, i => Assert.Equal(0, i), 
                                     i => Assert.Equal(1, i),
                                     i => Assert.Equal(2, i),
                                     i => Assert.Equal(3, i),
                                     i => Assert.Equal(4, i));
        }

        [Fact(DisplayName = "Range(n, step) should contain all elements from 0 to n - 1 at distance step")]
        public void Range_n_step_should_contain_all_elements_from_0_to_n_minus_1_at_distance_step()
        {
            var range = Range(5, (uint)2);
            Assert.True(3 == range.Count());
            Assert.Collection(range, i => Assert.Equal(0, i), 
                                     i => Assert.Equal(2, i),
                                     i => Assert.Equal(4, i));
        }

        [Fact(DisplayName = "Range(n, m) should contain all elements from n to m - 1")]
        public void Range_n_m_should_contain_all_elements_from_n_to_m_minus_1()
        {
            var range = Range(5, 10);
            Assert.True(5 == range.Count());
            Assert.Collection(range, i => Assert.Equal(5, i), 
                                     i => Assert.Equal(6, i),
                                     i => Assert.Equal(7, i),
                                     i => Assert.Equal(8, i),
                                     i => Assert.Equal(9, i));
        }

        [Fact(DisplayName = "Range(n, m, step) should contain all elements from n to m - 1 at distance step")]
        public void Range_n_m_should_contain_all_elements_from_n_to_m_minus_1_at_distance_step()
        {
            var range = Range(5, 10, 2);
            Assert.True(3 == range.Count());
            Assert.Collection(range, i => Assert.Equal(5, i), 
                                     i => Assert.Equal(7, i),
                                     i => Assert.Equal(9, i));
        }

        [Fact(DisplayName = "Range(-n) should contain all elements from 0 to -n + 1")]
        public void Range_minus_n_should_contain_all_elements_from_0_to_minus_n_plus_1()
        {
            var range = Range(-5);
            Assert.True(5 == range.Count());
            Assert.Collection(range, i => Assert.Equal(0, i), 
                                     i => Assert.Equal(-1, i),
                                     i => Assert.Equal(-2, i),
                                     i => Assert.Equal(-3, i),
                                     i => Assert.Equal(-4, i));
        }

        [Fact(DisplayName = "Range(-n, step) should contain all elements from 0 to -n + 1 at distance step")]
        public void Range_minus_n_step_should_contain_all_elements_from_0_to_minus_n_plus_1_at_distance_step()
        {
            var range = Range(-5, step: 2);
            Assert.True(3 == range.Count());
            Assert.Collection(range, i => Assert.Equal(0, i), 
                                     i => Assert.Equal(-2, i),
                                     i => Assert.Equal(-4, i));
        }

        [Fact(DisplayName = "Range(n, m) with n greater than m should count backwards from n to m + 1")]
        public void Range_n_m_with_n_greater_than_m_should_count_backwards_from_n_to_m_plus_1()
        {
            var range = Range(10, 5);
            Assert.True(5 == range.Count());
            Assert.Collection(range, i => Assert.Equal(10, i), 
                                     i => Assert.Equal(9, i),
                                     i => Assert.Equal(8, i),
                                     i => Assert.Equal(7, i),
                                     i => Assert.Equal(6, i));
        }

        [Fact(DisplayName = "Range(n, m, step) with n greater than m should count backwards from n to m + 1 at distance step")]
        public void Range_n_m_with_n_greater_than_m_should_count_backwards_from_n_to_m_plus_1_ad_distance_step()
        {
            var range = Range(10, 5, 2);
            Assert.True(3 == range.Count());
            Assert.Collection(range, i => Assert.Equal(10, i), 
                                     i => Assert.Equal(8, i),
                                     i => Assert.Equal(6, i));
        }
#endregion
    
        [Fact(DisplayName = "Add in order of IComparables builds a list in ascending order")]
        public void Add_in_order_of_IComparables_builds_a_list_in_ascending_order()
        {
            var ordered = new List<int> { 1, 2, 3, 4, 5 };
            var toOrder = new List<int>();

            toOrder.AddInOrder(3);
            toOrder.AddInOrder(5);
            toOrder.AddInOrder(1);
            toOrder.AddInOrder(2);
            toOrder.AddInOrder(4);

            Assert.True(ordered.SequenceEqual(toOrder));
        }

        [Fact(DisplayName = "Add in order of IComparables builds a linked list in ascending order")]
        public void Add_in_order_of_IComparables_builds_a_linked_list_in_ascending_order()
        {
            var ordered = new LinkedList<int>();
            ordered.AddLast(1);
            ordered.AddLast(2);
            ordered.AddLast(3);
            ordered.AddLast(4);
            ordered.AddLast(5);

            var toOrder = new LinkedList<int>();

            toOrder.AddInOrder(3);
            toOrder.AddInOrder(5);
            toOrder.AddInOrder(1);
            toOrder.AddInOrder(2);
            toOrder.AddInOrder(4);

            Assert.True(ordered.SequenceEqual(toOrder));
        }

        [Fact(DisplayName = "Add in order of items builds a list in the order specified by the comparer")]
        public void Add_in_order_of_items_builds_a_list_in_the_order_specified_by_the_comparer()
        {
            var ordered = new List<int> { 5, 4, 3, 2, 1 };
            IComparer<int> comparer = Comparer<int>.Create((i1, i2) => i2 - i1);
            var toOrder = new List<int>();

            toOrder.AddInOrder(comparer, 3);
            toOrder.AddInOrder(comparer, 5);
            toOrder.AddInOrder(comparer, 1);
            toOrder.AddInOrder(comparer, 2);
            toOrder.AddInOrder(comparer, 4);

            Assert.True(ordered.SequenceEqual(toOrder));
        }

        [Fact(DisplayName = "Add in order of items builds a linked list in the order specified by the comparer")]
        public void Add_in_order_of_items_builds_a_linked_list_in_the_order_specified_by_the_comparer()
        {
            var ordered = new LinkedList<int>();
            ordered.AddLast(5);
            ordered.AddLast(4);
            ordered.AddLast(3);
            ordered.AddLast(2);
            ordered.AddLast(1);
            IComparer<int> comparer = Comparer<int>.Create((i1, i2) => i2 - i1);
            var toOrder = new LinkedList<int>();

            toOrder.AddInOrder(comparer, 3);
            toOrder.AddInOrder(comparer, 5);
            toOrder.AddInOrder(comparer, 1);
            toOrder.AddInOrder(comparer, 2);
            toOrder.AddInOrder(comparer, 4);

            Assert.True(ordered.SequenceEqual(toOrder));
        }
    }
}