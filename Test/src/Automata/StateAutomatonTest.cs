using System;
using System.Collections.Generic;
using Xunit;
using Rebar.Automata;

namespace Test.Automata
{
    public class StateAutomatonTest
    {
#region Inner classes
        private class CountingState : IState
        {
            private readonly int _to;
            private readonly bool _resetting;
            private readonly List<int> _partials;
            private readonly string _name;

            public int Count { get; private set; }
            public string Name => _name;
            public bool IsActive { get; set; }
            public bool HasEnded => Count == _to;
            public IReadOnlyList<int> Partials => _partials;

            public CountingState(int to, bool resetting, string name) 
            {
                _to = to;
                _resetting = resetting;
                _partials = new List<int>();
                _name = name;
                Count = 0;
            }

            public void OnEnter()
            {
                if (_resetting) Count = 0;
            }

            public void OnExit() => _partials.Add(Count);

            public void OnTick()
            {
                if (!IsActive) return;
                Count++;
            }
        }
#endregion

        private const string NAME = "State";

        [Fact(DisplayName="Ticking an empty automaton throws InvalidOperationException")]
        public void Ticking_an_empty_automaton_throws_InvalidOperationException()
        {
            var automaton = new StateAutomaton();
            Assert.Throws<InvalidOperationException>(() => automaton.Tick());
        }

        [Fact(DisplayName="Adding two conditional transitions with the same starting and ending state should rais an exception")]
        public void Adding_two_conditional_transitions_with_the_same_starting_and_ending_state_should_rais_an_exception()
        {
            var automaton = new StateAutomaton();
            var state1 = new CountingState(5, false, NAME);
            var state2 = new CountingState(5, false, "SECOND");
            var state3 = new CountingState(5, false, "THIRD");

            automaton.AddConditionalTransition(state1, state2, () => true);

            Assert.Throws<ArgumentException>(() => automaton.AddConditionalTransition(state1, state2, () => false));

            automaton.AddConditionalTransition(state1, state3, () => true);
        }

        [Fact(DisplayName="Assigning an entry state results in that state becoming current state")]
        public void Assigning_an_entry_state_results_in_that_state_becoming_current_state()
        {
            var state1 = new CountingState(5, false, NAME);
            var automaton1 = new StateAutomaton(state1);

            var state2 = new CountingState(5, false, NAME);
            var automaton2 = new StateAutomaton();
            automaton2.EntryState = state2;

            Assert.Null(automaton1.CurrentState);
            Assert.Null(automaton2.CurrentState);

            Assert.Equal(NAME, automaton1.EntryState.Name);
            Assert.Equal(NAME, automaton2.EntryState.Name);

            automaton1.Tick();
            automaton2.Tick();

            Assert.Equal(NAME, automaton1.CurrentState.Name);
            Assert.Equal(NAME, automaton2.CurrentState.Name);
        }

        [Fact(DisplayName="A state ticks once when ticking the automaton")]
        public void A_state_ticks_once_when_ticking_the_automaton()
        {
            var state = new CountingState(5, false, NAME);
            var automaton = new StateAutomaton(state);

            automaton.Tick();

            Assert.Equal(1, state.Count);
        }

        [Fact(DisplayName="A sequential transition happens when a state has ended")]
        public void A_sequential_transition_happens_when_a_state_has_ended()
        {
            var state1 = new CountingState(2, false, NAME);
            var state2 = new CountingState(3, false, "SECOND");
            var automaton = new StateAutomaton(state1);

            automaton.SetSequentialTransition(state1, state2);

            automaton.Tick();
            automaton.Tick();
            automaton.Tick();

            Assert.Equal(state2, automaton.CurrentState);
            Assert.True(state1.HasEnded);
        }

        [Fact(DisplayName="A general transition happens when a give predicate is true starting from any state")]
        public void A_general_transition_happens_when_a_give_predicate_is_true_starting_from_any_state()
        {
            var state1 = new CountingState(1000, false, NAME);
            var state2 = new CountingState(1000, false, "SECOND");
            var fromAny = new CountingState(1, false, "ANY");
            var automaton = new StateAutomaton(state1);

            bool condition = false;
            automaton.AddGeneralTransition(fromAny, () => condition);
            automaton.SetSequentialTransition(fromAny, state2);

            automaton.Tick();
            automaton.Tick();

            Assert.Equal(state1, automaton.CurrentState);

            condition = true;
            automaton.Tick();

            Assert.Equal(fromAny, automaton.CurrentState);

            condition = false;
            automaton.Tick();

            Assert.Equal(state2, automaton.CurrentState);

            automaton.Tick();

            Assert.Equal(state2, automaton.CurrentState);

            condition = true;
            automaton.Tick();

            Assert.Equal(fromAny, automaton.CurrentState);
        }
    
        [Fact(DisplayName="A general transition cannot happen from the target state")]
        public void A_general_transition_cannot_happen_from_the_target_state() 
        {
            var fromAny = new CountingState(1000, true, "ANY");
            var automaton = new StateAutomaton(fromAny);

            bool condition = false;
            automaton.AddGeneralTransition(fromAny, () => condition);

            automaton.Tick();

            condition = true;
            automaton.Tick();

            Assert.Equal(2, fromAny.Count);
        }
    
        [Fact(DisplayName="A sequential transition happens only when the predicate is true")]
        public void A_sequential_transition_happens_only_when_the_predicate_is_true()
        {
            var state1 = new CountingState(1000, true, NAME);
            var state2 = new CountingState(2, false, NAME + "_2");
            var automaton = new StateAutomaton(state1);

            bool condition = false;
            automaton.AddConditionalTransition(state1, state2, () => condition);

            automaton.Tick();

            Assert.Equal(state1, automaton.CurrentState);

            condition = true;

            automaton.Tick();

            Assert.Equal(state2, automaton.CurrentState);
        }
    }
}