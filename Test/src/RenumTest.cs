using System.Reflection;
using System;
using Xunit;

using Rebar;
using System.Linq;

namespace Test
{
    public class RenumTest
    {
        private enum TestEnum { Value1, Value2, Value3 }

        [Fact(DisplayName = "Count is equivalent to getting values and asking for Length")]
        public void Count_is_equivalent_to_getting_values_and_asking_for_Length() =>
            Assert.Equal(Enum.GetValues(typeof(TestEnum)).Length, Renum.Count<TestEnum>());

        [Fact(DisplayName = "Values is equivalent to using GetValues")]
        public void Values_is_equivalent_to_using_GetValues()
        {
            var values = Enum.GetValues(typeof(TestEnum)).Cast<TestEnum>().ToArray();
            Assert.Collection(Renum.Values<TestEnum>(), e => Assert.Equal(values[0], e),
                                                        e => Assert.Equal(values[1], e),
                                                        e => Assert.Equal(values[2], e));
        }

        [Fact(DisplayName = "Parsing valid name returns the right enum value")]
        public void Parsing_valid_name_returns_the_right_enum_value() =>
            Assert.Equal(TestEnum.Value1, Renum.Parse<TestEnum>("Value1"));

        [Fact(DisplayName = "Parsing invalid name throws exceptions")]
        public void Parsing_invalid_name_throws_exceptions()
        {
            Assert.Throws<ArgumentNullException>(() => Renum.Parse<TestEnum>(null));
            Assert.Throws<ArgumentException>(() => Renum.Parse<TestEnum>("Wobbly"));
        }

        [Fact(DisplayName = "Try parsing valid name returns the right enum value and true")]
        public void Try_parsing_valid_name_returns_the_right_enum_value_and_true()
        {
            Assert.True(Renum.TryParse<TestEnum>("Value1", out TestEnum value));
            Assert.Equal(TestEnum.Value1, value);
        }

        [Fact(DisplayName = "Try parsing invalid name returns false")]
        public void Try_parsing_invalid_name_returns_false()
        {
            Convert.ToInt32(TestEnum.Value1);
            Assert.False(Renum.TryParse<TestEnum>(null, out TestEnum _));
            Assert.False(Renum.TryParse<TestEnum>("Wobbly", out TestEnum _));
        }

        [Fact(DisplayName = "Parsing or default valid name returns the right enum value")]
        public void Parsing_or_default_valid_name_returns_the_right_enum_value() =>
            Assert.Equal(TestEnum.Value1, Renum.ParseOrDefault<TestEnum>("Value1", TestEnum.Value3));

        [Fact(DisplayName = "Parsing or default invalid name returns default")]
        public void Parsing_or_default_invalid_name_returns_default()
        {
            Assert.Equal(TestEnum.Value3, Renum.ParseOrDefault<TestEnum>(null, TestEnum.Value3));
            Assert.Equal(TestEnum.Value3, Renum.ParseOrDefault<TestEnum>("Wobbly", TestEnum.Value3));
        }
    }
}