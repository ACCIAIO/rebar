using System;
using Xunit;

using Rebar.Events;

namespace Test.Events
{
    public class PubSubBoardTest
    {
        [Fact(DisplayName="Test an empty Subscriber and trigger")]
        public void Test_empty_subscribe_and_trigger()
        {
            var psh = new PubSubBoard();
            bool called = false;
            Action callback = () => called = true;
            psh.Subscribe("event", callback);
            psh.Trigger("event");
            psh.Unsubscribe("event", callback);
            Assert.True(called);
        }

        [Fact(DisplayName="Test a Subscriber and trigger")]
        public void Test_subscribe_and_trigger()
        {
            var psh = new PubSubBoard();
            bool called = false;
            Action<bool> callback = v => called = v;
            psh.Subscribe("event", callback);
            psh.Trigger("event", true);
            psh.Unsubscribe("event", callback);
            Assert.True(called);
        }

        [Fact(DisplayName="Test different Subscriber and trigger")]
        public void Test_different_subscribe_and_trigger()
        {
            var psh = new PubSubBoard();
            bool called = false;
            Action callback = () => called = true;
            psh.Subscribe("event1", callback);
            psh.Trigger("event2");
            psh.Unsubscribe("event1", callback);
            Assert.False(called);
        }

        [Fact(DisplayName="Test same name but different type Subscriber and trigger")]
        public void Test_different_type_but_same_name_Subscribe_and_trigger()
        {
            var psh = new PubSubBoard();
            bool called = false;
            Action callback = () => called = true;
            psh.Subscribe("event", callback);
            psh.Trigger("event", 5);
            psh.Unsubscribe("event", callback);
            Assert.False(called);
        }
    }
}