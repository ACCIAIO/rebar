#!/bin/bash

dotnet publish Rebar/Rebar.csproj

if [[ $? -eq 0 ]]
then
    echo "Publishing succeed"

    path="Rebar/bin/Debug/${NET_VERSION}/publish/Rebar_${BITBUCKET_BUILD_NUMBER}.dll"

    mv "Rebar/bin/Debug/${NET_VERSION}/publish/Rebar.dll" "$path"

    curl -X POST "https://${PIPELINE_AUTH}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$path"
    if [[ $? -eq 0 ]]
    then
        echo "Upload succeed"
        exit
    else
        echo "Upload failed"
        exit 1
    fi
fi

echo "Publishing failed"
exit 2
